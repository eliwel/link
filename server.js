// Imports
const express = require('express')
const bodyParser = require('body-parser')

// Variables
const PORT = process.env.PORT || 3000
const app = express()

// Routes
// const userRouter = require('./routes/user')

// Serve the app
app
  .use(bodyParser.json())
  .listen(PORT, () => console.log(`Server is running on port ${PORT}`))
