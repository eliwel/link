import React, { Component } from 'react'
import '../styles/queueItem.css'

class QueueItem extends Component {
    render() {
        return (
            <div className="queueItem">
                <div className="queueItemInfo">
                    <em>
                        <div><b>Title: </b>Release Item</div>
                        <div><b>Developer: </b>First Last</div>
                        <div><b>QA: </b>First Last</div>
                    </em>
                </div>
                <div className="queueItemInfo">
                    <div><em><b>Notes: </b>This is the last comment that was written on the item.</em></div>
                </div>
            </div>
        )
    }
}

export default QueueItem