import React, { Component } from 'react'
import '../styles/nav.css'

class Nav extends Component {

  render() {
    return (
      <div className="nav">
        <a href="/settings">Settings</a>
        <a href="/home">Logout</a>
      </div>
    )
  }
}

export default Nav;