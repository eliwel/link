import React, { Component } from 'react'
import Queue from './Queue'
import Nav from './Nav'
import '../styles/app.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
        <Queue />
      </div>
    )
  }
}

export default App
