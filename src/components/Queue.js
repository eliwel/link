import React, { Component } from 'react'
import QueueItem from './QueueItem'
import '../styles/queue.css'

class Queue extends Component {
    render() {
        const environments = ["Staging", "QA", "Integration", "Production"]

        return (
            <div className="queue">
                {environments.map(environment => 
                    <div className="environment">
                        <div><h1>{environment}</h1></div>
                        <QueueItem props={environment}/>
                    </div>
                )}
            </div>
        )
    }
}

export default Queue