const Store = require('openrecord/store/postgres')

const store = new Store({
  host: 'localhost',
  user: 'my-user',
  password: 'superSecret!',
  database: 'posts'
})

store
  .connect()
  .ready()
  .then()
  .close(callback)